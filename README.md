# ZoomViewer
An interactive zooming image viewer for Wikimedia Commons.

## Dependencies

* [iipsrv](https://github.com/ruven/iipsrv) installed in /fcgi-bin/iipsrv.fcgi
* [libvips binary tools](https://www.libvips.org/)

## Deployment

After pushing new code to GitLab, update the custom image with:

```
toolforge build start https://gitlab.wikimedia.org/toolforge-repos/zoomviewer
toolforge webservice restart
```

## Examples

Load the interactive zoomviewer for the commons file _Chicago.jpg_ using

* https://zoomviewer.toolforge.org/?f=Chicago.jpg

Use the JavaScript viewer by appending the `flash=no` parameter

* https://zoomviewer.toolforge.org/?f=Chicago.jpg&flash=no

Pull the IIIF information JSON manifest for the commons file _Chicago.jpg_ using

* https://zoomviewer.toolforge.org/iiif.php?f=Chicago.jpg
