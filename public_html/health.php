<?php

header('Content-Type: text/plain; charset=utf-8');
$ch = curl_init('http://localhost:' . getenv('PORT') . '/fcgi-bin/iipsrv.fcgi');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$body = curl_exec($ch);
$status = curl_getinfo($ch, CURLINFO_RESPONSE_CODE);
// The welcome page is delivered as a 400 error
if ($status == 400 || $status == 200) {
  echo "iipsrv OK\n";
} else {
  http_response_code(500);
  echo "iipsrv FAILED with code $status\n";
  echo htmlspecialchars( $body );
}
