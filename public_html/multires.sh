#!/bin/bash
cd `dirname $0`/cache

MD5=$1
FILE=$2

# fetch the full res file
TMP=${MD5}.jpg
MD51=`echo $MD5 | cut -c1`
MD52=`echo $MD5 | cut -c1-2`
wget -O "$TMP" "https://upload.wikimedia.org/wikipedia/commons/${MD51}/${MD52}/${FILE}" > /dev/null 2>&1

# wget sets the timestamp to the Last-Modified time, but we don't want prune.sh to delete it immediately
touch "$TMP"

# generate multiresolution pyramid
TIFF=${MD5}.tif
vips im_vips2tiff "$TMP" "$TIFF":jpeg:75,tile:256x256,pyramid
